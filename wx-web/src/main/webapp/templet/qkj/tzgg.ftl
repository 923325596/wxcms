<dl class="fly-panel fly-list-one">
    <dt class="fly-panel-title">通知公告</dt>
<#if articlelist ? exists>
    <#list articlelist as article>
        <#if article_index lt 15>
        <dd>
            <a href="${channel.channel_catalog+article.article_pk+".html"}">${article.article_title}</a>
            <span><i class="iconfont icon-pinglun1"></i> ${article.article_sendtime?substring(0,10)}</span>
        </dd>
        </#if>
    </#list>
</#if>
</dl>