<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>青海互助金泉青稞酒酿造有限公司</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="青海互助金泉青稞酒酿造有限公司">
    <link rel="stylesheet" href="/res/layui/css/layui.css">
    <link rel="stylesheet" href="/res/css/global.css">
</head>

<body>

    <!-- 网页头部 -->
    <div class="fly-header layui-bg-black">
        <!--#include virtual="/include/top.html" -->
    </div>
    <!-- 头部结束 -->

    <!-- 滚动图片 -->
    <div class="layui-container">
        <div class="fly-panel">
            <div class="layui-carousel" id="test1">
                <div carousel-item>
                    <!--#include virtual="/pages/comm/carousel.html" -->
                </div>
            </div>
        </div>
    </div>
    <!-- 滚动图片结束 -->
    <div class="layui-container">
        <blockquote class="layui-elem-quote">${article.article_title}</blockquote>
        <div class="fly-panel">
            ${article.article_content}
        </div>
    </div>
    <!--友情链接-->
    <div class="fly-panel">
        <!--#include virtual="/pages/comm/link.html"-->
    </div>


    <div class="fly-footer">
        <!--#include virtual="/include/footer.html" -->
    </div>

    <script src="/res/layui/layui.js"></script>
    <script>
        layui.use('carousel', function() {
            var carousel = layui.carousel;
            //建造实例
            carousel.render({
                elem: '#test1',
                width: '100%' //设置容器宽度
                    ,
                height: '240px',
                arrow: 'always' //始终显示箭头
                    //,anim: 'updown' //切换动画方式
            });
        });

        layui.use('element', function(){
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        
        //监听导航点击
        element.on('nav(demo)', function(elem){
            //console.log(elem)
            layer.msg(elem.text());
        });
        });
    </script>
</body>

</html>